import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        int[] primes = new int[5];
        primes[0] = 2;
        primes[1] = 3;
        primes[2] = 5;
        primes[3] = 7;
        primes[4] = 11;

        System.out.println("The first prime number is: " + primes[0]);

        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("John", "Jane"));
        friends.add("Chloe");
        friends.add("Zoe");
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of: " + inventory);


    }
}